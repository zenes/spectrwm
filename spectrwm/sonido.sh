#!/bin/bash

CURRENT_STATE=`amixer get Master | egrep 'Playback.*?\[o' | egrep -o '\[o.+\]'`
echo $CURRENT_STATE

if [[ $CURRENT_STATE =~ "[on]" ]]; then
    echo "entro"
    amixer set Master mute
else
    amixer set Master unmute
fi
