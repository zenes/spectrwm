#!/bin/bash
# Example Bar Action Script for Linux.
# Requires: acpi, iostat, lm-sensors, aptitude.
# Tested on: Debian Testing
# This config can be found on github.com/linuxdabbler

##############################
#     KERNEL
##############################

############################## 
#	    DATE
##############################

date() {
	  date="$(date +"%a, %b %d %R")"
	    echo -e "$date"
    }

############################## 
#	    DISK
##############################

hdd() {
	  hdd="$(df -h /home | grep /dev | awk '{print $3 " / " $5}')"
	    echo -e "  $hdd"
    }
##############################
#	    RAM
##############################

mem() {
	mem="$(free -h | awk '/Mem:/ {printf $3}')"
	echo -e " $mem"
}
##############################	
#	    CPU
##############################

cpu() {
	  read cpu a b c previdle rest < /proc/stat
	    prevtotal=$((a+b+c+previdle))
	      sleep 0.5
	        read cpu a b c idle rest < /proc/stat
		  total=$((a+b+c+idle))
		    cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
		      echo -e " $cpu%"
	      }
##############################	
#	    NETWORK
##############################
network() {
  disconnected=""
  wireless_connected=""
  ethernet_connected=""
  ID="$(ip link | awk '/state UP/ {print $2}')"
    if (ping -c 1 1.1.1.1) &>/dev/null; then
        if [[ $ID == e* ]]; then
            echo -e "$ethernet_connected" ;
        else
            echo -e "$wireless_connected" ;
        fi
    else
        echo -e "$disconnected" ;
    fi
}
##############################
#	    VOLUME
##############################

vol() {
	vol="$(amixer -c 2 get Master | awk -F'[][]' 'END{ print" " $2 }')"
	echo -e " $vol"
}
##############################
#	    Packages
##############################

# pkgs() {
	# pkgs="$(apt list --installed | wc -l)"
	# echo -e " Packages: $pkgs "
# }
############################## #	    UPGRADES
##############################

# upgrades() {
	# upgrades="$(aptitude search '~U' | wc -l)"
	# echo -e " Upgrades: $upgrades "
# }
##############################
#	    BATTERY
##############################

battery() {
  BATT=$( acpi -b | sed 's/.*[charging|unknown|Full], \([0-9]*\)%.*/\1/gi' )
  STATUS=$( acpi -b | sed 's/.*: \([a-zA-Z]*\),.*/\1/gi' )
  if [$STATUS == "Charging" ]; then
    echo -e " $BATT %"
  else
    echo -e " $BATT %"
  fi
}
##############################
#	    VPN
##############################

vpn() {
  vpn="$(ip a | grep tun0 | grep inet | wc -l)"
  echo -e "  $vpn"
}
# WEATHER
# weather() {
	# wthr="$(sed 20q ~/.config/weather.txt | grep value | awk '{print $2 $3}' | sed 's/"//g')"
	# echo " $wthr"
# }
#
# TEMP
# temp() {
	# tmp="$(grep temp_F ~/.config/weather.txt | awk '{print $2}' | sed 's/"//g' | sed 's/,/ F/g')"
	# echo " $tmp" # } #loops forever outputting a line every SLEEP_SEC secs 
while :; do     
  #This bar is for spectrwm 3.3+
  echo "+@fg=5;$(cpu)   +@fg=2;$(mem)   +@fg=1;$(hdd)   +@fg=3;$(vpn)   +@fg=1;$(vol)   +@fg=4;$(battery)   +@fg=2;$(network)"
  #This bar is for spectrwm 3.2 and lower
    #echo "$(cpu) | $(mem) | $(pkgs) | $(hdd) | $(vpn) | $(vol) | $(date)"
  sleep 0.1
done


