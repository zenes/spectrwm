# DEPENDENCIES
- (AUR) ttf-material-design-icons
- (AUR) otf-monacob-git
- redshift
- nitrogen
- spectrwm
- alacritty
- qutebrowser
